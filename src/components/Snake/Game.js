import React, { useRef, useEffect, useReducer, useCallback } from 'react'
import Snake from './Snake'
import Food from './Food'

const getRandomCoordinates = () => {
  const min = 1
  const max = 98
  const x = Math.floor((Math.random() * (max - min + 1) + min) / 2) * 2
  const y = Math.floor((Math.random() * (max - min + 1) + min) / 2) * 2
  return [x, y]
}

const initialState = {
  food: getRandomCoordinates(),
  score: 0,
  speed: 180,
  direction: 'RIGHT',
  snakeDots: [
    [0, 0],
    [2, 0],
  ],
  running: true,
}

function reducer(state, action) {
  switch (action.type) {
    case 'set':
      return { ...state, ...action.data }
    case 'reset':
      return { ...initialState }
    default:
      return { ...state }
  }
}

export default function Game() {
  const [state, dispatch] = useReducer(reducer, initialState)
  const setState = (data) => dispatch({ type: 'set', data })
  const resetState = (data) => {
    dispatch({ type: 'reset' })
    setState({ food: getRandomCoordinates() })
  }
  const didMountRef = useRef(false)
  const stateRef = useRef(state)

  const onInput = (e) => {
    e = e || window.event

    const { direction } = stateRef.current
    switch (e.keyCode) {
      case 38:
        direction !== 'DOWN' && setState({ direction: 'UP' })
        break
      case 40:
        direction !== 'UP' && setState({ direction: 'DOWN' })
        break
      case 37:
        direction !== 'RIGHT' && setState({ direction: 'LEFT' })
        break
      case 39:
        direction !== 'LEFT' && setState({ direction: 'RIGHT' })
        break
      default:
        break
    }
  }

  const moveSnake = useCallback(() => {
    const { running, direction, snakeDots } = stateRef.current
    if (!running) return

    const dots = [...snakeDots]
    let head = dots[dots.length - 1]

    switch (direction) {
      case 'RIGHT':
        head = [head[0] + 2, head[1]]
        break
      case 'LEFT':
        head = [head[0] - 2, head[1]]
        break
      case 'DOWN':
        head = [head[0], head[1] + 2]
        break
      case 'UP':
        head = [head[0], head[1] - 2]
        break
      default:
        break
    }

    dots.push(head)
    dots.shift()
    setState({ snakeDots: dots })
  }, [])

  const onGameOver = useCallback(() => {
    const { running } = stateRef.current
    // alert(`Game Over`)
    if (running) setState({ running: false })
  }, [])

  const checkIfOutOfBorders = useCallback(() => {
    const { snakeDots } = stateRef.current
    const head = snakeDots[snakeDots.length - 1]
    if (head[0] >= 100 || head[1] >= 100 || head[0] < 0 || head[1] < 0) {
      onGameOver()
    }
  }, [onGameOver])

  const checkIfCollapsed = useCallback(() => {
    const { snakeDots } = stateRef.current
    const snake = [...snakeDots]
    const head = snake[snake.length - 1]
    snake.pop()
    snake.forEach((dot) => {
      if (head[0] === dot[0] && head[1] === dot[1]) {
        onGameOver()
      }
    })
  }, [onGameOver])

  const enlargeSnake = () => {
    const { snakeDots } = stateRef.current
    let newSnake = [...snakeDots]
    newSnake.unshift([])
    setState({ snakeDots: newSnake })
  }

  const increaseSpeed = () => {
    const { speed } = stateRef.current
    if (speed > 10) setState({ speed: speed - 10 })
  }

  const checkIfEat = () => {
    const { food, snakeDots, score } = stateRef.current
    let head = snakeDots[snakeDots.length - 1]
    if (head[0] === food[0] && head[1] === food[1]) {
      setState({ score: score + 1 })
      setState({ food: getRandomCoordinates() })
      enlargeSnake()
      increaseSpeed()
    }
  }

  useEffect(() => {
    stateRef.current = state

    if (!didMountRef.current) {
      didMountRef.current = true
      setInterval(moveSnake, state.speed)
      document.onkeydown = onInput
    } else {
      checkIfOutOfBorders()
      checkIfCollapsed()
      checkIfEat()
    }
  })

  return (
    <div class="snack-game">
      <div className="board mx-auto mt-auto">
        <Snake snakeDots={state.snakeDots} />
        <Food dot={state.food} />
      </div>
      <div className="mx-auto mb-auto mt-3 text-center">
        {!state.running && <strong className="text-danger">Game Over!</strong>} Score: {state.score}
        <br />
        <button className="btn btn-primary btn-sm mt-3" onClick={() => resetState()}>
          Restart
        </button>
      </div>
    </div>
  )
}
