import React from 'react'

export default React.memo((props) => (
  <div
    className="snake-food"
    style={{
      left: `${props.dot[0]}%`,
      top: `${props.dot[1]}%`,
    }}
  ></div>
))
