import React, { Component } from 'react'
import Game from 'components/Snake/Game'
import 'sass/_app.scss'

export default class App extends Component {
  render() {
    return (
      <>
        <Game />
      </>
    )
  }
}
